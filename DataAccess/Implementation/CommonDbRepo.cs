﻿using DatabaseSvc.EntityModels.CommonDb;
using Microsoft.EntityFrameworkCore;

namespace DatabaseSvc.DataAccess.Implementation
{
    public class CommonDbRepo
    {
        private readonly ILogger<CommonDbRepo> _logger;
        private readonly CommonDbContext _dbContext;


        public CommonDbRepo(ILogger<CommonDbRepo> logger, CommonDbContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
        }


        public async Task<List<Machine>> GetMachines()
        {
            try
            {
                return await _dbContext.Machines.ToListAsync<Machine>();
            }
            catch (Exception ex)
            {
                throw;
            } 
        }

        public async Task<List<Module>> GetModules()
        {
            try
            {
                return await _dbContext.Modules.ToListAsync<Module>();
            }
            catch(Exception ex)
            {
                throw;
            } 
        }

        public async Task<List<ProductionLine>> GetProductionLines()
        {
            try {
                return await _dbContext.ProductionLines.ToListAsync<ProductionLine>();
            }
            catch(Exception ex)
            {
                throw;
            } 
        }
    }
}
