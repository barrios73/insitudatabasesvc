﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using DatabaseSvc.EntityModels;
using DatabaseSvc.Models;
using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Collections;
using System.Text.Json;


namespace DatabaseSvc.DataAccess.Implementation
{
    public class PredResultRepo
    {
        //private InSituDbContext _dBContext = new InSituDbContext();
        private readonly InSituDbContext _dBContext;

        public PredResultRepo(InSituDbContext dbContext)
        {
            _dBContext = dbContext;
        }

        public async Task<int> SaveResult(PredictionResult result)
        {
            var parameters = new List<SqlParameter>();


            parameters.Add(new SqlParameter("@Topic", result.Topic));
            parameters.Add(new SqlParameter("@MachineId", result.MachineId));
            parameters.Add(new SqlParameter("@ProductId", result.ProductId));
            parameters.Add(new SqlParameter("@ProcessId", result.ProcessId));
            parameters.Add(new SqlParameter("@WOId", result.WOId));
            parameters.Add(new SqlParameter("@WorkpieceId", result.WorkpieceId));
            parameters.Add(new SqlParameter("@PredictResult", result.PredictResult));
            parameters.Add(new SqlParameter("@IsOOS", result.IsOOS));
            parameters.Add(new SqlParameter("@CorrelationId", result.CorrelationId));
            parameters.Add(new SqlParameter("@CreatedDateTime", result.CreatedDateTime));

            Message<int> msg = new Message<int>();

            try
            {
                var rowsSaved = await _dBContext
                .Database
                .ExecuteSqlRawAsync("exec usp_Insitu_SavePredictionResult " +
                "@Topic, @MachineId, @ProductId, @ProcessId, @WOId, @WorkpieceId, @PredictResult, @IsOOS, @CorrelationId, @CreatedDateTime",
                parameters.ToArray());

                return rowsSaved;
            }
            catch (Exception ex)
            {
                throw;
            }

        }


        public Task<int> SaveRawData(PredictionRawData data)
        {
            return Task.Run(() =>
            {

                for (int i = 0; i < data.InputParamNames.Length; i++)
                {
                    RawData rawdata = new RawData();
                    rawdata.CorrelationId = data.CorrelationId;
                    rawdata.ParameterName = data.InputParamNames[i];
                    rawdata.Data = JsonSerializer.Serialize(data.Data[i]);
                    rawdata.CreatedDateTime = DateTime.Now;

                    _dBContext.RawDatas.Add(rawdata);
                }

                _dBContext.SaveChanges();

                return 1;
            });
        }


        public int GetDashboardStartTime()
        {
            int startime = 8;
            return startime;
        }


        public Task<List<TbResult>> GetCurrentPredictionResult(DateTime dashboardDate)
        {
            return Task.Run(() =>
            {
                List<TbResult> msg = new List<TbResult>();

                try
                {
                    SqlParameter startdate = new SqlParameter("@StartDate", dashboardDate);
                    List<TbResult> result = _dBContext
                            .TbResults
                            .FromSql($"EXECUTE dbo.usp_Insitu_GetPredictionResult {startdate}")
                            .ToList<TbResult>();

                    return result;
                }
                catch (Exception ex)
                {
                    throw;
                }
            });
        }


        public Message<DataTable> GetDailyOOSTrend(SearchCriteria criteria)
        {
            Message<DataTable> msg = new Message<DataTable>();
            
            try
            {
                using (var command = _dBContext.Database.GetDbConnection().CreateCommand())
                {                    
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "usp_Insitu_GetDailyYield";

                    if (criteria.Process != null)
                    {
                        SqlParameter param1 = new SqlParameter("@ProcessId", criteria.Process);
                        command.Parameters.Add(param1);
                    }

                    if (criteria.Product != null)
                    {
                        SqlParameter product = new SqlParameter("@ProductId", criteria.Product);
                        command.Parameters.Add(product);
                    }

                    if (criteria.Machine != null)
                    {
                        SqlParameter machine = new SqlParameter("@MachineId", criteria.Machine);
                        command.Parameters.Add(machine);
                    }

                    if (criteria.StartDate != null)
                    {
                        SqlParameter startdate = new SqlParameter("@StartDate", criteria.StartDate);
                        command.Parameters.Add(startdate);
                    }

                    if (criteria.EndDate != null)
                    {
                        SqlParameter endate = new SqlParameter("@EndDate", criteria.EndDate);
                        command.Parameters.Add(endate);
                    }

                    _dBContext.Database.OpenConnection();
                    using (var result = command.ExecuteReader())
                    {
                        DataTable dt = new DataTable();
                        dt.Load(result);

                        msg.HasError = false;
                        msg.Msg = "No err";
                        msg.Data = dt;
                    }
                }

            }
            catch (Exception ex)
            {
                msg.HasError = true;
                msg.Msg = ex.Message;
                msg.Data = new DataTable();
            }

            return msg;
        }


        public Message<List<TbResult>> GetPredictionResultsByWO(string workOrderNo, string processId)
        {
            Message<List<TbResult>> msg = new Message<List<TbResult>>();

            try
            {
                List<TbResult> result = _dBContext.TbResults
                .Where(x => x.ProcessId == processId && x.WOId == workOrderNo)
                .ToList();

                msg.HasError = false;
                msg.Msg = "No errors";
                msg.Data = result;


            }
            catch (Exception ex)
            {
                List<TbResult> result = new List<TbResult>();
                msg.HasError = false;
                msg.Msg = "No errors" + ex.Message;
                msg.Data = result;
            }

            return msg;
        }


        private string CreateNamedParamList(string namedParamList, string paramName)
        {
            if (namedParamList == "")
            {
                namedParamList = namedParamList + "@" + paramName;
            }
            else
            {
                namedParamList = namedParamList + ",@" + paramName;
            }

            return namedParamList;
        }


        //this is part of the query to search raw data
        //this will get the raw data by parameter name and date range, which will merge
        //in PredResultSvc later
        public Task<List<RawData>> GetRawData(SearchRawDataCriteria request)
        {
            return Task.Run(() =>
            {
                List<RawData> rawDataList = _dBContext.RawDatas.Where(x => (request.ParameterName == null || x.ParameterName == request.ParameterName)
                    && (request.StartDate == null || x.CreatedDateTime >= request.StartDate)
                    )
                .OrderByDescending(x => x.CreatedDateTime).Take(3000).OrderBy(x => x.CreatedDateTime)
                .ToList<RawData>();

                return rawDataList;
            });
        }


        public Task<List<TbResult>> GetRawDataBatchInfo(SearchRawDataCriteria request)
        {
            return Task.Run(() =>
            {
                List<TbResult> result = _dBContext.TbResults.Where(x => x.Topic == request.Topic
                   && (request.MachineId == null || x.MachineId == request.MachineId)
                   && (request.ProductId == null || x.ProductId == request.ProductId)
                   && (request.StartDate == null || x.CreatedDateTime >= request.StartDate)
                    && (request.EndDate == null || x.CreatedDateTime <= request.EndDate))
                .OrderByDescending(x => x.CreatedDateTime).Take(3000).OrderBy(x => x.CreatedDateTime)
                .ToList<TbResult>();

                return result;
            });
        }


        public Task<List<TbResult>> GetHistPredictionResults(SearchCriteria criteria)
        {
            return Task.Run(() =>
            {
                try
                {
                    List<TbResult> result = _dBContext.TbResults.Where(x =>
                        (criteria.Process == null || x.ProcessId == criteria.Process) &&
                        (criteria.Product == null || x.ProductId == criteria.Product) &&
                        (criteria.Machine == null || x.MachineId == criteria.Machine) &&
                        (criteria.StartDate == null || x.CreatedDateTime == criteria.StartDate) &&
                        (criteria.EndDate == null || x.CreatedDateTime == criteria.EndDate)
                    ).Take(1000).ToList();

                    return result;
                }
                catch (Exception ex)
                {
                    throw;
                }
            });

        }


    }
}
