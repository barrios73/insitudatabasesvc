﻿using DatabaseSvc.EntityModels;
using DatabaseSvc.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace DatabaseSvc.DataAccess.Interface
{
    public interface IPredResultSvc
    {
        Task<Message<int>> SavePredictionResult(PredictionResult result);

        Task<Message<int>> SaveResult(PredictionResult result);

        //int GetDashboardStartTime();

        Task<Message<List<DtoPredictionResult>>> GetCurrentPredictionResult(DateTime dashboardDate, int? moduleId, int? prodLineId, int? machineKeyId);
    
        //Message<List<PassFailResultWO>> GetPredictionResultsByWO(string workOrderNo, string processId);
    }
}
