﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using DatabaseSvc.DataAccess.Implementation;
using DatabaseSvc.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Channels;

namespace DatabaseSvc
{
    public class RabbitMQWorkerService : BackgroundService
    {
        private readonly ILogger<RabbitMQWorkerService> _logger;
        private ConnectionFactory _connectionFactory;
        private IConnection _connection;
        private IModel _channel;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly PeriodicTimer timer = new(TimeSpan.FromMilliseconds(60000));

        public RabbitMQWorkerService(ILogger<RabbitMQWorkerService> logger, IServiceScopeFactory serviceScopeFactory)
        {
            _logger = logger;
            _serviceScopeFactory = serviceScopeFactory;
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                ConnectRabbitMQ();
                CheckRabbitMQConn(_connection);
            }
            catch (Exception ex)
            {
                _logger.LogError("DatabaseSvc: Error in starting RabbitMQ background svc - " + ex.Message);
            }

            return base.StartAsync(cancellationToken); 
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            ProcessMsg();
            await Task.CompletedTask;

        }


        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            await base.StopAsync(cancellationToken);
            _connection.Close();
            _logger.LogInformation("RabbitMQ connection is closed.");
        }



        private async Task CheckRabbitMQConn(IConnection conn)
        {
            while (await timer.WaitForNextTickAsync())
            {

                if (conn.IsOpen)
                {
                    _logger.LogInformation("RabbitMQ conn OK: " + DateTime.Now);

                }
                else
                {
                    _logger.LogInformation("RabbitMQ conn NOK: " + DateTime.Now);
                    _logger.LogInformation("Code: " + conn.CloseReason.ReplyCode.ToString() + "\n Cause: " + conn.CloseReason.Cause);
                    conn.Dispose();

                    bool reconnect = false;

                    while (!reconnect)
                    {

                        try
                        {
                            ConnectRabbitMQ();
                            await ProcessMsg();
                            reconnect = true;
                        }
                        catch (Exception ex)
                        {
                            reconnect = false;
                            _logger.LogInformation("Retry connection");
                            Thread.Sleep(30000);
                        }
                    }

                    CheckRabbitMQConn(_connection);

                }
            }
        }


        private void ConnectRabbitMQ()
        {
            //string rabbitMQUri = "amqp://schan:insituquality@192.168.137.1:5672";
            string rabbitMQUri = Environment.GetEnvironmentVariable("RABBITMQ_URI");
            _connectionFactory = new ConnectionFactory
            {
                Uri = new Uri(rabbitMQUri),
                //DispatchConsumersAsync = true
            };

            _connectionFactory.AutomaticRecoveryEnabled = true;

            _connection = _connectionFactory.CreateConnection();

            _logger.LogInformation("RabbitMQ connection is opened.");

            _channel = _connection.CreateModel();

            _channel.ExchangeDeclare(exchange: "dashboard",
                        type: ExchangeType.Direct,
                        durable: true,
                        autoDelete: false);

            _channel.QueueDeclare(queue: "database",
                durable: true, //was true
                exclusive: false,
                autoDelete: false,
                //arguments: new Dictionary<string, object> { { "x-queue-type", "quorum" },
                //    { "x-delivery-limit",4} }
                arguments: null
            );

            _channel.QueueBind(queue: "database",
                                exchange: "dashboard",
                                routingKey: "dashboard");

            _channel.QueueBind(queue: "database",
                    exchange: "dashboard",
                    routingKey: "rawdata");

            _channel.BasicQos(prefetchSize: 0, prefetchCount: 10, global: false);


            if (ConfigSettings.CPPS == true)
            {
                _channel.QueueBind(queue: "tracking",
                                  exchange: "tracking",
                                  routingKey: "quality");
            }

        }


        private async Task ProcessMsg()
        {
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (sender, e) =>
            {
                var body = e.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                ulong msgId = e.DeliveryTag;

                //for quorum queue
                //_channel.BasicAck(e.DeliveryTag, false);

                try
                {
                    if (e.RoutingKey == "dashboard")
                    {
                        PredictionResult result = JsonSerializer.Deserialize<PredictionResult>(message);
                        _logger.LogInformation($"DatabaseSvc, CorrelationId: {result.CreatedDateTime}, Message {msgId}, consumed");

                        //for quorum queue
                        //_channel.BasicAck(e.DeliveryTag, true);
                        //if (e.BasicProperties.IsHeadersPresent())
                        //{
                        //    _logger.LogInformation($"Ack (rawdata): CorrelationId: {result.CorrelationId}, Delivery Count: {e.BasicProperties.Headers["x-delivery-count"]}");
                        //}

                        using (var scope = _serviceScopeFactory.CreateScope())
                        {
                            var myScopedService = scope.ServiceProvider.GetService<PredResultSvc>();

                            Message<int> msg = myScopedService.SavePredictionResult(result).Result;
                            if (msg.HasError == true)
                            {
                                string errMsg = msg.Msg;
                                _logger.LogError($"DatabaseSvc, CorrelationId: {result.CorrelationId}, Message {msgId}, error saving prediction result: ({errMsg})");
                            }
                            else
                            {
                                _logger.LogInformation($"DatabaseSvc, CorrelationId: {result.CorrelationId}, Message {msgId}, prediction result saved");
                            }
                        }
                    }
                    else if (e.RoutingKey == "rawdata")
                    {
                        PredictionRawData rawdata = JsonSerializer.Deserialize<PredictionRawData>(message);

                        //for quorum queue
                        //if (e.BasicProperties.IsHeadersPresent())
                        //{
                        //    _logger.LogInformation($"Ack (rawdata): CorrelationId: {rawdata.CorrelationId}, Delivery Count: {e.BasicProperties.Headers["x-delivery-count"]}");
                        //}

                        _logger.LogInformation($"DatabaseSvc, CorrelationId: {rawdata.CorrelationId}, Message {msgId}, consumed");

                        using (var scope = _serviceScopeFactory.CreateScope())
                        {
                            var myScopedService = scope.ServiceProvider.GetService<PredResultSvc>();

                            Message<int> msg = myScopedService.SaveRawData(rawdata).Result;
                            if (msg.HasError == true)
                            {
                                string errMsg = msg.Msg;
                                _logger.LogError($"DatabaseSvc, CorrelationId: {rawdata.CorrelationId}, Message {msgId}, error saving prediction result: ({errMsg})");
                            }
                            else
                            {
                                _logger.LogInformation($"DatabaseSvc, CorrelationId: {rawdata.CorrelationId}, Message {msgId}, prediction result saved");
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    //temporarily set requeue = false.  So if there is any exception, message is discarded
                    //since there is no dead letter exchange
                    //if (e.BasicProperties.IsHeadersPresent())
                    //{
                    //    _logger.LogInformation($"Nack - Message: {message.ToString()}, Delivery Count: {e.BasicProperties.Headers["x-delivery-count"]}");
                    //}

                    //_channel.BasicNack(e.DeliveryTag, false, true);
                }



            };


            //set autoAck to false because of quorum queue migration
            _channel.BasicConsume("database", true, consumer);
        }

    }
}
