﻿namespace DatabaseSvc.Models
{
    public class SearchRawDataResult
    {
        public string Topic { get; set; }
        public string MachineId { get; set; }
        public string ProductId { get; set; }
        public string ProcessId { get; set; }
        public string WOId { get; set; }
        public string CorrelationId { get; set; }
        public string ParameterName { get; set; }
        public string Data { get; set; }

        public string Result { get; set; }
        public DateTime CreatedDateTime { get; set; }
    }
}
