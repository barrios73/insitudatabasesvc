﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatabaseSvc.Models
{
    public class PassFailResultWO
    {
        public string ProductId { get; set; } 
        public string ProcessId { get; set; } 
        public string WorkOrderNo { get; set; } 
        public string WorkpieceId { get; set; } 
        public int OOS { get; set; }
    }
}
