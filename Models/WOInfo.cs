﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatabaseSvc.Models
{
    public class WOInfo
    {
        public string WOId { get; set; }
        public string ProductId { get; set; }
        public string ProcessId { get; set; }
        public string MachineId { get; set; }
        public int TotalQty { get; set; }
    }
}
