﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DatabaseSvc.EntityModels
{
    public partial class MlmodelType
    {
        public int Id { get; set; }
        public string ModelName { get; set; }
        public string Library { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public bool? IsActive { get; set; }
    }
}
