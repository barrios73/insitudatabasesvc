﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DatabaseSvc.EntityModels
{
    public partial class TbConfig
    {
        public int ConfigId { get; set; }
        public string ConfigName { get; set; }
        public string ConfigValue { get; set; }
        public string Remark { get; set; }
        public string StatusInd { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
    }
}
