﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DatabaseSvc.EntityModels
{
    public partial class MlmodelFile
    {
        public int Id { get; set; }
        public int ModelTypeId { get; set; }
        public string ModelFile { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public bool? IsActive { get; set; }
    }
}
