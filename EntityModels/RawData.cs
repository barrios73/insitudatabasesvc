﻿namespace DatabaseSvc.EntityModels
{
    public class RawData
    {
        public long Id { get; set; }
        public string CorrelationId { get; set; }
        public string ParameterName { get; set; }
        public string Data { get; set; }
        public DateTime CreatedDateTime { get; set; }
    }
}
