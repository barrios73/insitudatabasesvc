﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace DatabaseSvc.EntityModels
{
    public partial class InSituDbContext : DbContext
    {


        public InSituDbContext(DbContextOptions<InSituDbContext> options)
            : base(options)
        {
        }


        public virtual DbSet<TbResult> TbResults { get; set; }
        public virtual DbSet<RawData> RawDatas { get; set; }


//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
//                optionsBuilder.UseSqlServer("Data Source = tcp:192.168.137.1, 1433; Initial Catalog = InSituDB; User ID = InSitu; Password = insituanomaly");
//                //optionsBuilder.UseSqlServer("Server=shopfloor-mssql-latest;Database=InSituDB;user id=sa;password=Toughpass1!");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            //modelBuilder.Entity<MlmodelConfig>(entity =>
            //{
            //    entity.ToTable("MLmodelConfig");

            //    entity.HasIndex(e => e.Topic, "UC_Topic")
            //        .IsUnique();

            //    entity.Property(e => e.CreatedBy).HasMaxLength(100);

            //    entity.Property(e => e.CreatedDate).HasColumnType("datetime");

            //    entity.Property(e => e.LastModifiedBy).HasMaxLength(100);

            //    entity.Property(e => e.LastModifiedDate).HasColumnType("datetime");

            //    entity.Property(e => e.Topic)
            //        .IsRequired()
            //        .HasMaxLength(200);
            //});

            //modelBuilder.Entity<MlmodelFile>(entity =>
            //{
            //    entity.ToTable("MLModelFile");

            //    entity.HasIndex(e => e.ModelFile, "UC_ModelFile")
            //        .IsUnique();

            //    entity.Property(e => e.CreatedBy).HasMaxLength(100);

            //    entity.Property(e => e.CreatedDate).HasColumnType("datetime");

            //    entity.Property(e => e.LastModifiedBy).HasMaxLength(100);

            //    entity.Property(e => e.LastModifiedDate).HasColumnType("datetime");

            //    entity.Property(e => e.ModelFile)
            //        .IsRequired()
            //        .HasMaxLength(200);
            //});

            //modelBuilder.Entity<MlmodelType>(entity =>
            //{
            //    entity.ToTable("MLModelType");

            //    entity.Property(e => e.CreatedBy).HasMaxLength(100);

            //    entity.Property(e => e.CreatedDate).HasColumnType("datetime");

            //    entity.Property(e => e.LastModifiedBy).HasMaxLength(100);

            //    entity.Property(e => e.LastModifiedDate).HasColumnType("datetime");

            //    entity.Property(e => e.Library).HasMaxLength(100);

            //    entity.Property(e => e.ModelName).HasMaxLength(200);

            //    entity.Property(e => e.Remarks).HasMaxLength(1000);
            //});

            //modelBuilder.Entity<PreprocessingFile>(entity =>
            //{
            //    entity.ToTable("PreprocessingFile");

            //    entity.Property(e => e.PreprocessingFile1)
            //        .IsRequired()
            //        .HasMaxLength(200)
            //        .HasColumnName("PreprocessingFile");
            //});

            //modelBuilder.Entity<TbConfig>(entity =>
            //{
            //    entity.HasKey(e => e.ConfigId)
            //        .HasName("PK_tb_Config_ConfigID");

            //    entity.ToTable("tb_Config");

            //    entity.Property(e => e.ConfigId).HasColumnName("ConfigID");

            //    entity.Property(e => e.ConfigName).HasMaxLength(50);

            //    entity.Property(e => e.ConfigValue).HasMaxLength(50);

            //    entity.Property(e => e.CreatedBy).HasMaxLength(20);

            //    entity.Property(e => e.CreatedDateTime).HasColumnType("datetime");

            //    entity.Property(e => e.ModifiedBy).HasMaxLength(20);

            //    entity.Property(e => e.ModifiedDateTime).HasColumnType("datetime");

            //    entity.Property(e => e.Remark).HasMaxLength(200);

            //    entity.Property(e => e.StatusInd)
            //        .HasMaxLength(1)
            //        .IsUnicode(false)
            //        .IsFixedLength(true);
            //});

            modelBuilder.Entity<TbResult>(entity =>
            {
                entity.HasKey(e => e.ResultId)
                    .HasName("PK_tb_Result_ResultID");

                entity.ToTable("tb_Result");

                entity.Property(e => e.ResultId).HasColumnName("ResultID");

                entity.Property(e => e.WOId)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedBy).HasMaxLength(20);

                entity.Property(e => e.CreatedDateTime).HasColumnType("datetime");

                entity.Property(e => e.IsOos).HasColumnName("IsOOS");

                entity.Property(e => e.MachineId).HasMaxLength(10);

                entity.Property(e => e.ModifiedBy).HasMaxLength(20);

                entity.Property(e => e.ModifiedDateTime).HasColumnType("datetime");

                entity.Property(e => e.ProcessId).HasMaxLength(100);

                entity.Property(e => e.ProductId).HasMaxLength(100);

                entity.Property(e => e.Result).HasMaxLength(50);

                entity.Property(e => e.StatusInd)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('Y')")
                    .IsFixedLength(true);

                entity.Property(e => e.WorkpieceId).HasMaxLength(10);

                entity.Property(e => e.CorrelationId);
            });

            modelBuilder.Entity<WorkCenter>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("WorkCenter");

                entity.Property(e => e.WorkCenter1)
                    .HasMaxLength(100)
                    .HasColumnName("WorkCenter");
            });

            modelBuilder.Entity<RawData>(entity =>
            {
                entity.ToTable("tb_RawData");

                entity.HasKey(e => e.Id);
                //entity.HasNoKey();

                entity.Property(e => e.CorrelationId).HasMaxLength(100);

                entity.Property(e => e.ParameterName).HasMaxLength(50);

                entity.Property(e => e.Data);

                entity.Property(e => e.CreatedDateTime).HasColumnType("datetime2");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
