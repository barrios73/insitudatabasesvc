﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DatabaseSvc.EntityModels.CommonDb
{
    public partial class Process
    {
        public int Id { get; set; }
        public string ProcessId { get; set; }
        public string ProcessName { get; set; }
    }
}
