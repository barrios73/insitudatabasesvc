﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DatabaseSvc.EntityModels.CommonDb
{
    public class ProductionLine
    {
        public int Id { get; set; }
        public string Line { get; set; }
        public int ModuleId { get; set; }
    }
}
