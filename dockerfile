#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80

ENV RABBITMQ_URI "amqp://schan:insituquality@192.168.137.1:5672"
ENV INSITUDB_URL "Data Source = tcp:192.168.137.1, 1433; Initial Catalog = InSituDB; User ID = InSitu; Password = insituanomaly; Encrypt = false"
ENV COMMONDB_URL "Data Source = tcp:192.168.137.1, 1433; Initial Catalog = CommonDb; User ID = InSitu; Password = insituanomaly; Encrypt = false"

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["Database6.csproj", "."]
RUN dotnet restore "./Database6.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "Database6.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Database6.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Database6.dll"]